---
title: "Projet MRR Bio"

date: "07/11/2022"
output: pdf_document
---
Binome: 11
Data explored: Bio
```{r setup, include=FALSE}
rm(list=ls())
graphics.off()


library(plyr)

library(readr)

library(dplyr)

library(caret)

library(ggplot2)

library(repr)
library(bigstep)
```

## Data preparation

1)Loading the data 
First we need to load the data then we chose a target variable, in our case Amylose content which is a quantitive variable.
we regroup the explanatory variables and the response variable in a data frame named Data_Bio.

```{r }
load(file="D:\\2\\Regression\\project_park.RData")
Data_Bio=data.frame(pheno.df$Amylose.content,Xmat)
names(Data_Bio)[1] <- "Ybin"
```

2) Removing outliers
Explanatory variables can take either the value 0 (homozygous, major allele),the value 1 (heterozygous),the value 2 (homozygous, minor allele).
The following code detects and corrects variable values that exceed 2 or are inferior than 0.

```{r }
summary(Data_Bio)[1:6,500:512]
#for(i in 1:dim(Data_Bio)[2]){
 # if(summary(Data_Bio)[1,i]<0){Data_Bio[,i][Data_Bio[,i]==summary(Data_Bio)[1,i]]=NA}
  #if(summary(Data_Bio)[6,i]>2){Data_Bio[,i][Data_Bio[,i]==summary(Data_Bio)[6,i]]=NA}
#}
```
3) Missing value treatment

There are several techniques for handling missing values.We have chosen the mode method (we'll replace every 
 NA with the most occurring value )
```{r }

getmode<- function(v){
  uniqv<-unique(v)
  uniqv[which.max(tabulate(match(v,uniqv)))]
}
for(i in 2:dim(Data_Bio)[2]){
  Data_Bio[,i][is.na(Data_Bio[,i])]=getmode(Data_Bio[,i])
}
Data_Bio[,1][is.na(Data_Bio[,1])]=median(Data_Bio[,1],na.rm=TRUE)
``` 

4) Data Visualisation
```{r }
library(FactoMineR)
geno.pca=PCA(Data_Bio[,-1],scale.unit=FALSE)
plot(geno.pca$ind$coord[,1:2])
plot(geno.pca$ind$coord[,3:4])
title("Principal component analysis")

```
\emph{Figure 1}: Summary of the genetic data using the principal component analysis 



```{r }
head(geno.pca$var$coord)

```
\emph{Table  1}: some genetic markers and their correlation to the top five principle components

In order to visualize our data efficiently we have chosen to use the principal component analysis the top four principal components are illustrated in (Fig 1) as they explain the large majority of genetic variations.Indeed, we can see in (Tab 1) that genetic markers are strongly correlated with the first two principle components while their correlation with the fifth component is deemed insignificant. Moreover, we clearly observe that our individuals are separated in four distinct groups (Fig 1),each one of these groups is characterized by a set of genetic markers strongly correlated with the first and second principal component.
## Target variables
```{r }
library(corrplot)
corrplot(cor(pheno.df[,-1,-2]))

```
\emph{\centerline{Figure 2}}: Correlation between target variables

The phenotype data set contains 34 variables that are significantly correlated (Fig 2), and can be regrouped into 6 categories: Flowering time, Morphology,Yield components,Seed morphology,Stress tolerance and quality. We have set our target variable to be The Amylose content.The amylose content of the milled rice is the major determinant of rice texture hence its quality. In this study, we'll analyse and determine the genetic markers that have the most influence on the rice amylose content.

 
##Variable selection
In our case where the number of variables p=36901 largely surpasses the number of observations n= 413 (p>>n), the least square method is unusable.
This is why we've considered an approach involving a forward procedure in order to minimise the number of variables.We'll be using the forward selection because unlike backward elimination, forward selection can used when the number of variables under consideration is very large.
This is because forward selection starts with a null model (with no predictors) and proceeds to add variables one at a time, and so unlike backward selection, it does not have to consider the full model (which includes all the predictors).
The stepwise function (step()) couldn't handle our very large dataset,this is why we used the package "bigstep"to be able to use variable selection methods or even combinations of them.

At first we will use the fast_forward method which adds every variable that reduces a criterion (not necessarily the best one) and as we can see in the summary it has selected some variables that have the most influence on Amylose content.
First we need to split our data:
```{r}
library(caTools)
library(glmnet)
library(gglasso)
set.seed(101)
ind <- sample(c(TRUE, FALSE), dim(Data_Bio)[1], replace=TRUE, prob=c(0.75, 0.25))
train <- Data_Bio[ind, ]
test <- Data_Bio[!ind, ]
```

```{r }
for(i in 2:dim(train)[2]){
  train[,i][train[,i]==0]=3
  test[,i][test[,i]==0]=3
}
x_test1=test[,-1]
X1=train[,-1]
Y1=train$Ybin
#X1=scale(X1)
Y1=scale(Y1)
X2=test[,-1]
#X2=scale(X2)
Y2=test$Ybin
Y2=scale(Y2)
x_test=as.matrix(X2)
y_test=as.matrix(Y2)
x_train=as.matrix(X1)
y_train=as.matrix(Y1)
set.seed(101)
data <- prepare_data(y_train,x_train)
data %>%
  fast_forward(bic) ->
  results1
summary(results1)

```

```{r }
estimated_coeff = summary(results1)$coefficients[,1]
selected_variables=results1$Xm
y_predicted_train= selected_variables%*%estimated_coeff

plot(y_train,y_predicted_train)
abline(a=0,b=1,col="red",lwd=2)

selected_markers_forward=names(estimated_coeff)
```
This model has a multiple R-squared value of 0.85.
We notice that our model gives accurate results since forward regression tends toward over-fitting, which happens when we put in more variables than is actually good for the model; it typically shows a very close, neat
fit of the data used in regression.
```{r }
# predicted y using test data
r=c()
for(e in 2:dim(selected_variables)[2]){
  
  r=c(r,select(x_test1,colnames(selected_variables)[e]))
}
r=data.frame(r)
r=data.frame(rep(1,nrow(r)),r)
y_predicted_test=as.matrix(r)%*%estimated_coeff
plot(y_test,y_predicted_test)
#Rsquare and RMSE
d3=data.frame(
  RMSE = RMSE(y_predicted_test,y_test),
  Rsquare = R2(y_predicted_test,y_test)
)
colnames(d3)[2]<-"Rsquared"
d3
```

This model has a predictive R-squared value of 0.12.
so this model is far off from additional data points and not good for
prediction.
## Regression using Group Lasso 

 It has been demonstrated that gene expression data have cluster structure, where the clusters consist of co-regulated genes which tend to have coordinated functions.
That is why we have chosen an approach that takes into account the cluster structure in gene expression data (Group Lasso).
The Group Lasso is an extension of the lasso to do variable selection on (predefined) groups of variables in linear regression models.
Instead of choosing independent variables like the Lasso method(if there is a group of highly correlated variables, then the Lasso tends to select one variable from a group and ignore the others) ,to overcome this limitation the Group Lasso selects groups of variables.
In our case where gene expression data has no apparent biological cluster information, we first need to divide genes into clusters.For this matter we'll be using the kmeans clustering method.
The Kmeans algorithm tries to partition the dataset into K pre-defined distinct clusters where each data point belongs to only one group. It tries to make the intra-cluster data points as similar as possible while also keeping the clusters as different as possible.

Running the k-means with 2000 group:
```{r }
res.kmeans.bio=kmeans(t(Data_Bio[,-1]),2000,nstart=5,iter.max = 100)
kmeans.group=res.kmeans.bio$cluster

```

Cross validation using group lasso:

```{r warning=FALSE}
library(gglasso)


# 5-fold cross validation using group lasso
GLasso_Reg_cv=cv.gglasso(x=x_train,y=y_train,group=kmeans.group,loss="ls")

# the model with the least prediction error
Glasso.min<-gglasso(x=x_train,y=y_train,group=kmeans.group,
                lambda = GLasso_Reg_cv$lambda.min)

plot(GLasso_Reg_cv,xvar="lambda")
```
The plot displays the cross-validation error according to the log of lambda. The left vertical line shows that the log of the optimal value of lambda is approximately -3.75 , which is the one that minimizes the prediction error. This lambda value will give the most accurate model but also the model with the most predictors. 

The exact value of lambda min can be viewed as follow:
```{r}
lambda_min<-GLasso_Reg_cv$lambda.min
lambda_min
sprintf("Number of non zero coefficients: %d",length(coef(GLasso_Reg_cv,GLasso_Reg_cv$lambda.min)[coef(GLasso_Reg_cv,GLasso_Reg_cv$lambda.min)!=0]))
```
This value of lambda allows us to choose a large number of variables. We want to reduce that number down so we've considered a model with a higher value of lambda :
```{r warning=FALSE}
Glasso.se<-gglasso(x=x_train,y=y_train,group=kmeans.group,
                lambda = 4*GLasso_Reg_cv$lambda.1se)
```


```{r warning=FALSE}

Y_tild <- predict(Glasso.se,newx = x_test,s=4*GLasso_Reg_cv$lambda.1se)
# RMSE and R squared
d=data.frame(
  RMSE = RMSE(Y_tild,y_test),
  Rsquare = R2(Y_tild,y_test)
)
colnames(d)[2]<-"Rsquared"
d
```
This model has an $R^2$ of 0.69  which reveals that 69% of the variability observed in the target variable is explained by the regression model.

```{r warning=FALSE}
sprintf("This model leaves us with %d variables",length(Glasso.se$beta[Glasso.se$beta!=0]))
```


## Comparison
```{r warning=FALSE}

#all coeffiecients from group lasso method
co=Glasso.se$beta
ddd=data.frame(rownames(co),co)
#selected variables 
  selected_var_groupLasso=ddd[,1][ddd[,2]!=0]
#comparison
tt=rep(0,min(c(length(selected_var_groupLasso),length(selected_markers_forward))))
for(i in min(c(length(selected_var_groupLasso),length(selected_markers_forward)))){
  if(identical(tolower(selected_var_groupLasso[i]), tolower(selected_markers_forward[i]))){
    tt[i]=selected_var_groupLasso[i]
  }
}
sprintf("genes en commun")
tt

```
By comparing the genetic markers selected by each of the previous methods: we have found none in common.

Forward method fits the data well but has a very low predictive power.

The group Lasso method has a higher predictive power. However this method is based on the group selection provided by the k means algorithm.

Giving that the K means starts by choosing random centers, the groups generated are not optimal which affects the selectivity of the Group Lasso method.

To remedy this problem, one way is to apply the kmeans multiple times and then choose the best classification (based on the distance from each point to its center). Unfortunately given the enormity of the data set , we couldn't perform it.
